package com.bsd.mapper;

import com.bsd.domain.entity.SysDept;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 部门管理 Mapper 接口
 * </p>
 *
 * @author luLiFeng
 * @since 2020-01-14
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
