package com.bsd.controller;


import com.bsd.domain.entity.SysDept;
import com.bsd.service.SysDeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 部门管理 前端控制器
 * </p>
 *
 * @author luLiFeng
 * @since 2020-01-14
 */
@Controller
@RequestMapping("/sys-dept")
public class SysDeptController {

    @Autowired
    private SysDeptService sysDeptService;


    @RequestMapping("test")
    @ResponseBody
    public List<SysDept> test(){

        List<SysDept> list = sysDeptService.list();

        return list;
    }

}
