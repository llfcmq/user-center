package com.bsd.service.impl;

import com.bsd.domain.entity.SysDept;
import com.bsd.mapper.SysDeptMapper;
import com.bsd.service.SysDeptService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 部门管理 服务实现类
 * </p>
 *
 * @author luLiFeng
 * @since 2020-01-14
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

}
