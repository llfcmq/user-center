package com.bsd.service;

import com.bsd.domain.entity.SysDept;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bsd.mapper.SysDeptMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * 部门管理 服务类
 * </p>
 *
 * @author luLiFeng
 * @since 2020-01-14
 */
public interface SysDeptService extends IService<SysDept> {



}
